# Biblioteca Pessoal

Sistema para registro de livros pessoais

### Docker Hub

Link para o repositório do Docker Hub:
- [https://hub.docker.com/repository/docker/nilomendonca/trabalho-individual-2021-2](https://hub.docker.com/repository/docker/nilomendonca/trabalho-individual-2021-2).

### Ruby and Rails version 
- Ruby `3.0.0p0`  
- Rails `7.0.2.3`
- Bundler

### Dependências
Banco de dados Postgres

#### Configuração do Projeto - Docker
Para executar a aplicação com o Docker no localhost na porta 3000.
```
docker-compose up
```

#### Configuração do Projeto - Ruby
```
bundle install
```

#### Configuração do Banco
Criar o banco com o usuario `postgres` e senha `postgres`

#### Iniciatlização, criação do Banco com seed
```
rails db:create
rails db:migrate
rails db:seed
```

ou 

```
rails db:setup
```

#### Reset do banco e seed
```
rails db:reset
```

#### Como rodar a aplicação
Para executar a aplicação com o servidor web embutido basta rodar o comando abaixo que a mesma será inicializada no localhost na porta 3000.
```
rails server
```

#### Como rodar testes
```
rails db:reset
rspec
```

